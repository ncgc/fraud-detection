# Detecção de Fraude em comércio eletrônico

![Image](https://img.shields.io/badge/Machine%20Learning-Supervised-brightgreen)
![Image](https://img.shields.io/badge/Language-Python-blue)

Criação de um modelo de previsão com um algoritmo supervisionado e outro 
não-supervisionado, a fim de prever se alguém estará envolvido em uma atividade
fraudulenta no futuro ou não.

### :file_folder: Dados
**Fraud_Data.csv**

| coluna | descrição | tipo |
|---|---|---|
| id  | identificador único  | integer |   
| cadastro  | data e hora em que o cadastro do usuário foi realizado |  string |   
| compra  | data e hora em que a compra foi realizada  |  string |   
| valor  | valor da compra   | float |
| id_dispositivo  | identificador do dispositivo  | string |
| fonte  | canal de marketing utilizado de direcionamento do usuário | string |
| browser  | browser utilizado pelo usuário para realizar a compra  | string |
| genero    | gênero declarado pelo usuário | string |
| idade  |  idade declarada pelo usuário | integer |
| ip  | ip da máquina utilizada pelo usuário durante o ato de compra  | float |
| fraude  | classificação se houve ou não fraude  | integer |

**IpAddress_to_Country.csv**

 coluna | descrição | tipo |
|---|---|---|
| limite_inferior_ip  | limite inferior do endereço ip | float |   
| limite_superior_ip  | limite superior do endereço ip |  integer |   
| pais  | nacionalidade a que o ip pertence  |  string  |   

### :thought_balloon: Etapas do processo

- [x] Leitura dos arquivos (csv)
- [x] Limpeza dos dados

      -**Fraud_Data:** Mudança do tipo string para datetime nas colunas cadastro e compra
      -**IpAddress_to_Country:** Mudança do tipo float para integer na coluna limite_inferior_ip
      
- [x] Análise exploratória dos dados a fim de identificar variáveis mais relevantes para o modelo preditivo

      - Variáveis relevantes obtidos com o ExtraTreesClassifier:
            - Média de id_dispositivo
            - Tempo entre cadastro e compra
            
- [x] Criação de modelos preditivos

    - Isolation Forest
    

- [ ] Conclusões

----------------------------------------
*Código adaptado do curso Modelos Preditivos para Detecção de Fraude
ofertado pela Alura.*
    
